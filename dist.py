#!/usr/bin/python

import os
import re
import subprocess
import sys

# Versions List
versions = [ "1.6.6" ,       \
			 "1.6.7" ,       \
			 "1.6.8" ,       \
			 "1.6.9" ,       \
			 "1.6.11"       \
			 ]
json_file = 'package_Alligator_index.json'

# Store Dimension and SHA256
size=[None]*len(versions)
sha=[None]*len(versions)
DEBUG=False

# remove old dist
os.system("rm -rf dist 2> /dev/null")
os.system("mkdir dist")
# remove all .DS_Store
os.system("find . -name '*.DS_Store' -type f -delete")

# Check Arguments
if len(sys.argv) > 1:
	if sys.argv == '--debug' or sys.argv == '-d':
		DEBUG=True

for i in range(0,len(versions)):
	print "Create Archive Alligator "+str(versions[i])
	# Create this version archive
	os.system("tar -zcvf dist/alligator_"+str(versions[i])+".tar.gz alligator_"+str(versions[i])+" 2> /dev/null")
	# Get archive dimension
	p = subprocess.Popen(["stat", "-f%z","dist/alligator_"+str(versions[i])+".tar.gz"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	size[i], err = p.communicate()
	size[i] = size[i].replace("\n","")
	# Get archive SHA256
	p = subprocess.Popen(["shasum", "-a","256","dist/alligator_"+str(versions[i])+".tar.gz"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	sha[i], err = p.communicate()
	sha[i] = sha[i].replace("\n","")
	sha[i] = re.sub(r' .*', '', sha[i])

	if DEBUG:
		print str(size[i]) + " " + str(sha[i])

# Modify Json file with archive dimension and SHA256
buff=""
count=0
with open(json_file) as fp:
	for line in fp:
		if "checksum" in line and (count < len(versions) ):
			buff += "          \"checksum\":\"SHA-256:"+str(sha[count])+"\",\n"
		elif "size" in line and (count < len(versions)):
			buff += "          \"size\":\""+str(size[count])+"\",\n"
			count+=1
		else:
			buff+=line


# Export Json file to dist directory
f = open("dist/"+json_file,"w")
f.write(buff);
f.close()
print "Json " + json_file + " Exported"
